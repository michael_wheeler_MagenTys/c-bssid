#include <iostream>
#include <windows.devices.wifi.h>
#include <wlanapi.h>
#include <objbase.h>
#include <wtypes.h>

#include <stdio.h>
#include <stdlib.h>

// Need to link with Wlanapi.lib and Ole32.lib
#pragma comment(lib, "wlanapi.lib")
#pragma comment(lib, "ole32.lib")

using namespace std;

int main() 
{
	// Declare and initialize variables.

	HANDLE hClient = NULL;
	DWORD dwMaxClient = 2;
	DWORD dwCurVersion = 0;
	DWORD dwResult = 0;
	DWORD dwRetVal = 0;
	int iRet = 0;

	WCHAR GuidString[39] = { 0 };

	unsigned int i, j, k;

	/* variables used for WlanEnumInterfaces  */

	PWLAN_INTERFACE_INFO_LIST pIfList = NULL;
	PWLAN_INTERFACE_INFO pIfInfo = NULL;

	PWLAN_AVAILABLE_NETWORK_LIST pBssList = NULL;
	PWLAN_AVAILABLE_NETWORK pBssEntry = NULL;

	int iRSSI = 0;

	dwResult = WlanOpenHandle(dwMaxClient, NULL, &dwCurVersion, &hClient);
	if (dwResult != ERROR_SUCCESS) {
		cout << "Wlan open handle failed! with error: %u\n";
		return 1;
	}

	dwResult = WlanEnumInterfaces(hClient, NULL, &pIfList);
	if (dwResult != ERROR_SUCCESS) {
		cout << "Wlan enumerate interfaces failed! with error: %u\n";
		return 1;
	}

	else {
		wprintf(L"Num Entries: %lu\n", pIfList->dwNumberOfItems);
		wprintf(L"Current Index: %lu\n", pIfList->dwIndex);

		for (i = 0; i < (int)pIfList->dwNumberOfItems; i++) {
			pIfInfo = (WLAN_INTERFACE_INFO *)&pIfList->InterfaceInfo[i];
			wprintf(L"  Interface Index[%u]:\t %lu\n", i, i);
			iRet = StringFromGUID2(pIfInfo->InterfaceGuid, (LPOLESTR)&GuidString,
				sizeof(GuidString) / sizeof(*GuidString));

			if (iRet == 0)
				wprintf(L"StringFromGUID2 failed\n");
			else {
				wprintf(L"  InterfaceGUID[%d]: %ws\n", i, GuidString);
			}
			wprintf(L"  Interface Description[%d]: %ws", i,
				pIfInfo->strInterfaceDescription);
			wprintf(L"\n");
			
			WlanScan(hClient,
				&pIfInfo->InterfaceGuid,
				NULL,
				NULL,
				NULL);

			if (dwResult != ERROR_SUCCESS) {
				wprintf(L"WlanScan failed with error: %u\n",
					dwResult);
				dwRetVal = 1;
			}
			else {
				wprintf(L"WLANScanSuccess\n");
			}



			dwResult = WlanGetAvailableNetworkList(hClient,
				&pIfInfo->InterfaceGuid,
				0,
				NULL,
				&pBssList);

			/////////////////////////////////////////////////////////////////////////////////////////////////////

			if (dwResult != ERROR_SUCCESS) {
				wprintf(L"WlanGetAvailableNetworkList failed with error: %u\n",
					dwResult);
				dwRetVal = 1;
			}
			else {
				wprintf(L"WLAN_AVAILABLE_NETWORK_LIST for this interface\n");


				// NUMBER OF WLAN'S (WIFI CARDS)
				wprintf(L"  Num WLAN Entries: %lu\n\n", pBssList->dwNumberOfItems);

				// ITERATE THROUGH AVAILABLE NETWORKS
				for (j = 0; j < pBssList->dwNumberOfItems; j++) {
					pBssEntry =
						(WLAN_AVAILABLE_NETWORK *)& pBssList->Network[j];

					// NETWORK NAME
					wprintf(L"  Profile Name[%u]:  %ws\n", j, pBssEntry->strProfileName);

					// NETWORK SSID
					wprintf(L"  SSID[%u]:\t\t ", j);
					if (pBssEntry->dot11Ssid.uSSIDLength == 0)
						wprintf(L"\n");
					else {
						for (k = 0; k < pBssEntry->dot11Ssid.uSSIDLength; k++) {
							wprintf(L"%c", (int)pBssEntry->dot11Ssid.ucSSID[k]);
						}
						wprintf(L"\n");
					}

					//# BSSID
					wprintf(L"  Number of BSSIDs[%u]:\t %u\n", j, pBssEntry->uNumberOfBssids);

					// SIGNAL
					if (pBssEntry->wlanSignalQuality == 0)
						iRSSI = -100;
					else if (pBssEntry->wlanSignalQuality == 100)
						iRSSI = -50;
					else
						iRSSI = -100 + (pBssEntry->wlanSignalQuality / 2);

					wprintf(L"  Signal Quality[%u]:\t %u (RSSI: %i dBm)\n", j,
						pBssEntry->wlanSignalQuality, iRSSI);

					// FLAGS
					wprintf(L"  Flags[%u]:\t 0x%x", j, pBssEntry->dwFlags);
					if (pBssEntry->dwFlags) {
						if (pBssEntry->dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED)
							wprintf(L" - Currently connected");
						if (pBssEntry->dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED)
							wprintf(L" - Has profile");
					}
					wprintf(L"\n");

					wprintf(L"\n");
				}
			}
		}

	}
	if (pBssList != NULL) {
		WlanFreeMemory(pBssList);
		pBssList = NULL;
	}

	if (pIfList != NULL) {
		WlanFreeMemory(pIfList);
		pIfList = NULL;
	}
	cin.get();
	return dwRetVal;
}










/*
// print the basic information of a visible wireless network
VOID PrintNetworkInfo(
	__in PWLAN_AVAILABLE_NETWORK pNetwork
)
{
	WCHAR strSsid[DOT11_SSID_MAX_LENGTH + 1];

	if (pNetwork != NULL)
	{
		// SSID
		wcout << L"SSID: " << SsidToStringW(strSsid, sizeof(strSsid) / sizeof(WCHAR), &pNetwork->dot11Ssid) << endl;

		// whether security is enabled
		if (pNetwork->bSecurityEnabled)
		{
			wcout << L"\tSecurity enabled." << endl;
		}
		else
		{
			wcout << L"\tSecurity not enabled." << endl;
		}

		// number of BSSIDs
		wcout << L"\tContains " << pNetwork->uNumberOfBssids << L" BSSIDs." << endl;

		// whether have a profile for this SSID
		if (pNetwork->dwFlags & WLAN_AVAILABLE_NETWORK_HAS_PROFILE)
		{
			wcout << L"\tHas a matching profile: " << pNetwork->strProfileName << L"." << endl;
		}

		// whether it is connected
		if (pNetwork->dwFlags & WLAN_AVAILABLE_NETWORK_CONNECTED)
		{
			wcout << L"\tCurrently connected." << endl;
		}

		// whether it is connectable
		if (!pNetwork->bNetworkConnectable)
		{
			// the reason that it is not connectable
			wcout << L"\tThe network is not connectable. ";
			PrintReason(pNetwork->wlanNotConnectableReason);
		}
		else
		{
			wcout << L"\tThe network is connectable." << endl;
		}

		// BSS type
		wcout << L"\tBSS type: " << GetBssTypeString(pNetwork->dot11BssType) << endl;

		// Signal quality
		wcout << L"\tSignal quality: " << pNetwork->wlanSignalQuality << L"%" << endl;

		// Default auth algorithm
		wcout << L"\tDefault authentication algorithm: " << GetAuthAlgoString(pNetwork->dot11DefaultAuthAlgorithm) << endl;

		// Default cipher algorithm
		wcout << L"\tDefault cipher algorithm: " << GetCipherAlgoString(pNetwork->dot11DefaultCipherAlgorithm) << endl;
	}
}

*/